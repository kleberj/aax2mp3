# README #

## aax2mp3 ##

##  ##


    NAME
            aax2mp3 - convert aax audio book files (Audible) to mp3
                      or other audio formats supported by ffmpeg

    SYNOPSIS
            aax2mp3 [options] [-a authcode] -i inputfile

    COPYRIGHT
            aax2mp3 is Copyright (C) 2017,2019,2021 by Josef Kleber
            License: GPLv3

    DESCRIPTION
            aax2mp3 provides a user-friendly  interface to  ffmpeg to  convert audio
            books in aax format into other audio formats - especially mp3! It serves
            the backup of personally purchased audio books, not to convert any audio
            books! aax2mp3 doesn't crack the DRM of audio books! It just uses ffmpeg
            and your personal Audible authcode  to convert your audio books to other
            audio formats. See AUTHCODE, how to obtain your personal authcode.

    OPTIONS
        Generic Program Information
            -h, --help
                Output a usage message and exit.
            -V, --version
                Output the version number and exit.
            -v, --verbose
                Create more vorbose output.

        Audio Book Selection
            -i AUDIOBOOK, --input AUDIOBOOK, --audiobook AUDIOBOOK
                Select an audio book (aax format).
            -a AUTHCODE, --authcode AUTHCODE
                Set the authcode for ffmpeg conversion.

        Mode Selection
            -S, --single
                Output one big single audio file.
            -C, --chapter (default)
                Output the  audio book  divided into chapters.  This  probably  only
                works  for  the  mp3  format!  See  option  --postprocessing  for  a
                workaround!
            -p, --postprocessing
                Create intermediate mp3 files  and post process them to the desired
                audio format.
            -k, --keepsingle
                Keep the one big audio file in chapter mode.

        Output File Format
            -b BITRATE, --bitrate BITRATE
                Set the bitrate of output file (default: bitrate of input file).
            -c CODEC, --codec CODEC
                Set the codec of the output file. (default: libmp3lame)
                See: ffmpeg -encoders | grep -e '^ A'
                -> acc, flac, libopus, libvorbis, ...
            -e EXTENSION, --extension EXTENSION
                Set the file extension of the output file. (default: mp3)
                -> acc, flac, opus, ogg, ...

        Metadata (default: metadata of input file)
            -G GENRE, --genre GENRE
                Set genre metadata of output file.
            -T TITLE, --title TITLE
                Set title metadata of output file.
            -A ARTIST, --artist ARTIST
                Set artist metadata of output file.
            -B ALBUM, --album ALBUM
                Set album metadata of output file.
            -R ALBUM_ARTIST, --albumartist ALBUMARTIST
                Set album_artist metadata of output file.
            -M COMMENT, --comment COMMENT
                Set comment metadata of output file.
            -D DATE, --date DATE
                Set date metadata of output file.
            -Y COPYRIGHT, --copyright COPYRIGHT
                Set copyright metadata of output file.
            -I COVERIMAGE, --image COVERIMAGE
                Set cover image (JPG).

    ENVIRONMENT VARIABLE
        AAX2MP3_AUTHCODE
            Instead of using the -a option to set the authcode,  you can also set it
            using the AAX2MP3_AUTHCODE environment variable, e.g. by adding:
                export AAX2MP3_AUTHCODE="12345678"
            to your ~/.bash_profile

    AUTHCODE
            Any  player or  converter dealing  with aax files  do need  a so  called
            authcode. This is a personalized code embedded as DRM. There are several
            ways to obtain your personal Audible authcode. Google is your friend!
            You may be interested in:
            [1] https://github.com/inAudible-NG/tables
            [2] https://github.com/inAudible-NG/audible-activator

    EXAMPLES
        aax2mp3 -i B077NH5P9Q_ep7.aax -a 12345678 --single
            converts B077NH5P9Q_ep7.aax into one single mp3 file.

        aax2mp3 --audiobook B077NH5P9Q_ep7.aax -v -a 12345678 --title 'Titanic 03'
        --artist 'Titanic' --codec libvorbis --extension ogg --bitrate 160
        --postprocess
            converts B077NH5P9Q_ep7.aax into chapter divided ogg vorbis files with a
            bitrate of 160 kb/s and a few metadata changes.

    REPORTING BUGS
            https://bitbucket.org/kleberj/aax2mp3/issues
